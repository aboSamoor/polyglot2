.. _contact:

==================
Contact us
==================

We would love to hear from about how Polyglot2 has been useful to you. If you face issues, or have requests please contact us.
