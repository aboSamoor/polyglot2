#!/usr/bin/env python
# -*- coding: utf-8 -*-


from distutils.extension import Extension
from distutils.core import setup
import numpy as np 

have_cython = False
try:
    from Cython.Distutils import build_ext
    from Cython.Build import cythonize
    have_cython = True
except ImportError:
    from distutils.command.build_ext import build_ext

if False:
  ext_modules = [Extension("polyglot2_inner",
                 ["polyglot2/polyglot2_inner.pyx"], libraries=['blas'],
                 include_dirs = [np.get_include()],extra_link_args=['-L/usr/include'])]
else:
  ext_modules = [Extension("polyglot2_inner",
                 ["polyglot2/polyglot2_inner.c"], libraries=['blas'],
                 include_dirs = [np.get_include()],extra_link_args=['-L/usr/include'])]
setup(
  name='polyglot2',
  version='1.0',
  description='Deep learning suite for NLP',
  author='Rami Al-Rfou',
  author_email='rmyeid@gmail.com',
  url='https://bitbucket.org/aboSamoor/polyglot2',
  cmdclass={'build_ext': build_ext},
  ext_modules = ext_modules,
  classifiers=[
        'Topic :: Text Processing :: Linguistic',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Library (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Topic :: Scientific/Engineering :: Artificial Intelligence',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Topic :: Text Processing :: Linguistic',
      ],
  packages=['polyglot2'],
  scripts=['polyglot2/scripts/polyglot2-trainer.py']
)
